import scrapy
import re

class InformaSpider(scrapy.Spider):

    name = "informa_spider"

    def start_requests(self):
        index_url = getattr(self, "index_url", None)
        yield scrapy.Request(f"{index_url}?&from=0&size=48", self.parse_index)

    # def parse_index_list(self, response):
    #
    #     index_list = response.css(".section-category-list li a::attr(href)").extract()
    #
    #     for url in index_list:
    #         yield scrapy.Request(f"{url}&from=0&size=48", self.parse_index)


    def parse_index(self, response):

        product_list = response.css(".col-xs-3 .product-container a::attr(href)").extract()
        for url in product_list:
            yield scrapy.Request(url, self.parse_product_page)

        is_it_index = response.css(".icon-doubleright").extract()
        if is_it_index:
            next_page = response.css(".paggination-pcp ul .disabled .icon-right").extract()
            if len(next_page) == 0:
                current_url = response.url
                current_page = int(re.findall('\d+', current_url[current_url.find("from"):].split("&")[0])[0]) + 48
                final_url = f"{current_url[:str(current_url).find('from')]}from={current_page}&size=48"
                yield scrapy.Request(final_url, self.parse_index)


    def parse_product_page(self, response):

        try:
            title = response.css(".title-product::text").extract_first()
        except:
            title = None

        try:
            images = response.css(".fancybox .bitmap img::attr(src)").extract()
        except:
            images = None

        try:
            sku = response.css(".product-material ul li b::text").extract_first()
        except:
            sku = None

        try:
            price = int(response.css(".price::text").extract_first().replace("Rp", "").replace(".", "").strip())
        except:
            price = None

        try:
            old_price = int(response.css(".container .pdp-sticky .no-padding-left .pdp-action-right .price-box .price-old::text").extract_first().replace(".", "").replace("Rp", "").strip())
        except:
            old_price = price

        try:
            description = response.css(".product-desc").extract_first().replace("Ruparupa", "Daruma").replace("ruparupa.com", "daruma.co.id")
        except:
            description = None

        yield {
            'title': title,
            'images': images,
            'sku': sku,
            'price': price,
            'description': description,
            'product_url': response.url,
            'old_price': old_price
        }

