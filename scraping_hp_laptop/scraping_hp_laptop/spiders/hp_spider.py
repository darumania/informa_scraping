import scrapy

class HpSpider(scrapy.Spider):

    name = "hp_spider"

    varians_list = []

    def start_requests(self):
        index_url = getattr(self, "index_url", None)
        yield scrapy.Request(index_url, self.parse_index)

    def parse_index(self, response):

        url_list = response.css(".product-item-link::attr(href)").extract()

        for url in url_list:
            yield scrapy.Request(url, self.parse_product_page)

        next_page = response.css(".pages-item-next .next::attr(href)").extract_first()
        if next_page:
            yield scrapy.Request(next_page, self.parse_index)

    def parse_product_page(self, response):

        def checking_varians(varians_list, varians):
            for i in varians:
                if i not in varians_list:
                    return True
            return False

        try:
            title = response.css(".page-title .base::text").extract_first()
        except:
            title = None

        try:
            images = response.css(".fotorama__loaded--img::attr(href)").extract()
        except:
            images = None

        try:
            sku = response.css(".sku .value::text").extract_first().strip()
        except:
            sku = None

        try:
            price = int(float(response.css(".price-wrapper::attr(data-price-amount)").extract_first()))
        except:
            price = None

        try:
            description = response.css("table.additional-attributes").extract_first()
        except:
            description = None

        try:
            included = response.css(".included li::attr(title)").extract_first()
        except:
            included = None

        try:
            yield {
                'title': title,
                'images': images,
                'sku': sku,
                'price': price,
                'description': description,
                'included': included
            }
        finally:
            varians = response.css(".product-view-choose-item-list .list a::attr(data-url)").extract()
            if varians and checking_varians(self.varians_list, varians):
                for url in varians:
                    if url not in self.varians_list:
                        self.varians_list.append(url)
                        yield scrapy.Request(url, self.parse_product_page)

