import pymongo
import pprint
import time

myclient = pymongo.MongoClient("mongodb://172.22.0.2:27113/")
# myclient = pymongo.MongoClient("mongodb://46.101.77.153:27113/")

productsdb = myclient['scraping_items']
items = productsdb['items']

def insert_mim(data):
    return items.insert_one(data).acknowledged

def check_data(url):
    try:
        if items.find_one({'title': url}):
            return True
        else:
            return False
    except:
        return 0

def get_all_data():
    result = []
    for data in items.find():
        result.append(data)
    return result

def get_data_by_page_name(name):
    result = []
    for data in items.find({'page_name':name}):
        result.append(data)
    return result

def get_page_names():
    result = []
    for data in items.distinct("page_name"):
        if data in result:
            pass
        else:
            result.append(data)
    return result


#

# print(watching.update_many({}, {"$set": {"status":"uncheck"}}).acknowledged)

# pages = get_page_names()
# for page in get_page_names():
#     data = {
#         'page':f'https://www.facebook.com/{page}',
#         'status':'uncheck'
#     }
#     watching.insert(data)

# print(mims.delete_one({"test": "ya test"}))
#
# pprint.pprint(get_all_data())

print(len(get_all_data()))
