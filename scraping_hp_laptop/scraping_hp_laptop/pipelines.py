# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from .mongo_conn import *

class ScrapingHpLaptopPipeline(object):
    def process_item(self, item, spider):
        check = check_data(url=item['title'])
        if check == False:
            insert = insert_mim(item)
            if insert:
                spider.logger.info('item inserted')
            else:
                spider.logger.info('item not inserted')
        else:
            spider.logger.info('item already exists')
        return item
