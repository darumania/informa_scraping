import os
import time

os.chdir(f"{os.getcwd()}/scraping_hp_laptop")

urls = """
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/hp-essential-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/probook-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/elite-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/zbook-laptops.html
https://store.hp.com/id-id/default/desktops/business-desktops/hp-desktop.html
https://store.hp.com/id-id/default/desktops/business-desktops/pro-desktops.html
https://store.hp.com/id-id/default/desktops/business-desktops/elitedesk-desktops.html
https://store.hp.com/id-id/default/desktops/business-desktops/z-workstations.html
https://store.hp.com/id-id/default/desktops/business-desktops/thin-client.html
https://store.hp.com/id-id/default/laptops-tablets/personal-laptops/spectre-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/personal-laptops/envy-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/personal-laptops/omen-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/personal-laptops/pavilion-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/personal-laptops/hp-essential-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/elite-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/zbook-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/probook-laptops.html
https://store.hp.com/id-id/default/laptops-tablets/business-laptops/hp-essential-laptops.html
https://store.hp.com/id-id/default/desktops/personal-desktops/omen-desktops.html
https://store.hp.com/id-id/default/desktops/personal-desktops/pavilion-desktops.html
https://store.hp.com/id-id/default/desktops/personal-desktops/hp-essential-desktops.html
https://store.hp.com/id-id/default/desktops/business-desktops/elitedesk-desktops.html
https://store.hp.com/id-id/default/desktops/business-desktops/pro-desktops.html
https://store.hp.com/id-id/default/desktops/business-desktops/hp-desktop.html
https://store.hp.com/id-id/default/desktops/business-desktops/thin-client.html
https://store.hp.com/id-id/default/desktops/business-desktops/z-workstations.html
"""

printer_url = """
https://store.hp.com/id-id/default/printers/personal-and-home-printers/deskjet.html
https://store.hp.com/id-id/default/printers/personal-and-home-printers/ink-tank-system.html
https://store.hp.com/id-id/default/printers/personal-and-home-printers/laserjet.html
https://store.hp.com/id-id/default/printers/personal-and-home-printers/officejet.html
https://store.hp.com/id-id/default/printers/personal-and-home-printers/sprocket.html
https://store.hp.com/id-id/default/printers/business-printers/laserjet.html
https://store.hp.com/id-id/default/printers/business-printers/pagewide-pro.html
https://store.hp.com/id-id/default/printers/scanners.html
https://store.hp.com/id-id/default/printers/business-printers/designjet.html
"""

ink_urls = """
https://store.hp.com/id-id/default/ink-toner/ink/combo-twin-packs.html
https://store.hp.com/id-id/default/ink-toner/ink/ink-standard-packs.html
https://store.hp.com/id-id/default/ink-toner/ink/xl-packs.html
https://store.hp.com/id-id/default/ink-toner/toner/toner-dual-packs.html
https://store.hp.com/id-id/default/ink-toner/toner/toner-high-yield-cartridges.html
https://store.hp.com/id-id/default/ink-toner/toner/toner-standard-packs.html
"""

informa_urls = """
https://informa.co.id/
"""

url_clean = informa_urls.strip().split("\n")

time.sleep(30)

def crawl(url, index=None):
    os.system(f"scrapy crawl informa_spider -a index_url={url}")


url = "https://www.ruparupa.com/informa/by-room.html"


time.sleep(30)
crawl(url=url)

# for i in range(len(url_clean)):
#     crawl(url=url_clean[i], index=i)
