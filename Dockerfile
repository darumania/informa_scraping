FROM ubuntu:18.04

MAINTAINER Farhan "farhan@daruma.co.id"

RUN apt-get update && apt-get install -y \
    python3 python3-pip chromium-browser \
    wget unzip

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "run.py" ]
